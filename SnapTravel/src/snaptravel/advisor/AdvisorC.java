package snaptravel.advisor;

import io.indico.api.Indico;
import io.indico.api.exception.IndicoException;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import snaptravel.URLParser.URLParserC;

public class AdvisorC {

	String emotion;
	static Map<String,String> emotionToLocation = new HashMap<String,String>();
	
	public static void setEmotionToLocation(){
		emotionToLocation.put("Angry", "Calm");
		emotionToLocation.put("Sad", "Bars");
		emotionToLocation.put("Fear", "Religious");
		emotionToLocation.put("Surprise", "Unseen");
		emotionToLocation.put("Happy", "Restaurants,Game");
		emotionToLocation.put("Neutral", "Calm,Bar,Pub,Religious,Unseen,Restaurants,Game");
		
	}
	
	public AdvisorC(String strongestEmotion) {
		emotion = strongestEmotion;
	}

	public String advice() throws UnsupportedEncodingException, JSONException {
		String placeToTravel = emotionToLocation.get(emotion);
		URLParserC urlObject = new URLParserC();
		String result = urlObject.getJSONString("http://api.tripadvisor.com/api/partner/2.0/search/" + URLEncoder.encode(placeToTravel,"UTF-8") +
				"?key=HackUMass-93b8e93cda61");
		JSONObject resultObject = new JSONObject(result);
		JSONArray jsonArray = resultObject.getJSONArray("attractions");
		String finalAdvice = jsonArray.getJSONObject(0).getString("location_string");
		String finalAdviceAddress = jsonArray.getJSONObject(0).getJSONObject("address_obj").getString("address_string");
		String picsLink = jsonArray.getJSONObject(0).getString("see_all_photos");
		
		System.out.println("EMOTION : " + emotion);
		System.out.println(finalAdvice + " " + finalAdviceAddress);
		System.out.println(picsLink);
		//System.out.println(result);
		return finalAdvice + " " + finalAdviceAddress;
	}
	
	
}
