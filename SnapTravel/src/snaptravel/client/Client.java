package snaptravel.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import snaptravel.Base64.Base64;
import snaptravel.advisor.AdvisorC;

public class Client {
	
	    public static void main(String[] args) {
	        String jsonResult = post();
	        JSONObject objTemp;
	        JSONObject obj;
			try {
				objTemp = new JSONObject(jsonResult);
				obj = objTemp.getJSONObject("results");
				Map<String, Double> resultMap = new HashMap<String,Double>();
		        resultMap.put("Angry", 100 * obj.getDouble("Angry"));
		        resultMap.put("Sad", 100 * obj.getDouble("Sad"));
		        resultMap.put("Neutral", 100 * obj.getDouble("Neutral"));
		        resultMap.put("Surprise", 100 * obj.getDouble("Surprise"));
		        resultMap.put("Fear", 100 * obj.getDouble("Fear"));
		        resultMap.put("Happy", 100 * obj.getDouble("Happy"));
	
		        //display(resultMap);
		        String strongestEmotion = findStrongestEmotion(resultMap);
		        AdvisorC.setEmotionToLocation();
		        AdvisorC objadvisor = new AdvisorC(strongestEmotion);
		        objadvisor.advice();
		        
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
	    }

	    private static String findStrongestEmotion(Map<String, Double> resultMap) {
			
	    	String [] emotionStrings = {"Angry","Sad","Neutral","Surprise","Fear","Happy"};
	    	double [] emotions = new double [6];
			emotions[0] = resultMap.get("Angry");
			emotions[1] = resultMap.get("Sad");
			emotions[2] = resultMap.get("Neutral");
			emotions[3] = resultMap.get("Surprise");
			emotions[4] = resultMap.get("Fear");
			emotions[5] = resultMap.get("Happy");
			
			String semotion = emotionStrings[0];
			double max = emotions[0];
			for(int i = 1; i < 6; ++i){
				if(emotions[i] > max){
					max = emotions[i];
					semotion = emotionStrings[i]; 
				}
			}
			return semotion;
		}

		private static void display(Map<String, Double> resultMap) {
			System.out.println("Angry  " + resultMap.get("Angry"));
			System.out.println("Sad  " + resultMap.get("Sad"));
			System.out.println("Neutral  " + resultMap.get("Neutral"));
			System.out.println("Surprise  " + resultMap.get("Surprise"));
			System.out.println("Fear  " + resultMap.get("Fear"));
			System.out.println("Happy  " + resultMap.get("Happy"));
			
		}

		public static String post() {

			String jsonString = null;
			
	        @SuppressWarnings({ "deprecation", "resource" })
			HttpClient httpclient = new DefaultHttpClient();
	        HttpPost httppost = new HttpPost(
	        		"http://apiv2.indico.io/fer?key=f258c80ba09ce7d8ab6c703701b089c6");
	        try {	
	        
	        	List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
	            nameValuePairs.add(new BasicNameValuePair("data",
	            		Base64.encodeFromFile("data/replace.jpg")));
	            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	            HttpResponse response = httpclient.execute(httppost);
	            jsonString = EntityUtils.toString(response.getEntity());
	            //System.out.println("this is the response " + jsonString);

	            return jsonString;
	        } catch (ClientProtocolException e) {
	            System.out.println("CPE" + e);
	        } catch (IOException e) {
	            System.out.println("IOE" + e);
	        }
			return jsonString;
	        
	        
	    }
	}
	
	
	
	/*
	public static void main(String [] args){

		try {
			String encodedbase64 = Base64.encodeFromFile("data/sad.jpg");
			URLParserC obj = new URLParserC();
			JSONObject result = obj.convertToJSON("http:/​/​apiv2.indico.io/​fer?key=f258c80ba09ce7d8ab6c703701b089c6",encodedbase64);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}*/

