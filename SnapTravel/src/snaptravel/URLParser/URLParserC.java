package snaptravel.URLParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class URLParserC {

	public String getJSONString(String urlString){
		
		URL url;
		BufferedReader br = null;
		String JSONString = null; 
		try {
			// get URL content
			url = new URL(urlString);
			URLConnection conn = url.openConnection();
 
			// open the stream and put it into BufferedReader
			br  = new BufferedReader(
                               new InputStreamReader(conn.getInputStream()));
 
			String inputLine = null;
			StringBuilder sb = new StringBuilder();
			while ((inputLine = br.readLine()) != null) {
			sb.append(inputLine);
			}
			
			JSONString = sb.toString();
 
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return JSONString;
	}
}
